package com.app.recyclerviewdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    VolleySingleton volleySingleton;
    RequestQueue requestQueue;
    ArrayList<Demo> eventList = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    DemoAdapter demoAdapter;

    public static final String EVENT_URL = "http://test.multitechdevelopers.com/production/" +
            "source/request.php?function=getevent";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();

        linearLayoutManager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        recyclerView.setLayoutManager(linearLayoutManager);

        MainRequest();
    }

    public void MainRequest() {

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, EVENT_URL,
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                if (response != null) {
                    try {
                        JSONArray jsonArray = response.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            String eventName = object.getString("event_name");

                            Demo information = new Demo();
                            information.setEventName(eventName);

                            eventList.add(information);
                        }

                        demoAdapter = new DemoAdapter(MainActivity.this, eventList);

                        recyclerView.setAdapter(demoAdapter);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
//                    Log.d(TAG, "0 response");
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(jsonObjectRequest);

    }
}

package com.app.recyclerviewdemo;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Chirag on 30-06-2017.
 */

public class DemoAdapter extends RecyclerView.Adapter<DemoAdapter.DrawerViewHolder> {

    private LayoutInflater inflater;
    ArrayList<Demo> data = new ArrayList<>();
    Context context;
    public int row_data = -1;


    public DemoAdapter(Context context, ArrayList<Demo> data) {
        inflater = LayoutInflater.from(context);
        this.data = data;
        this.context = context;
    }


    @Override
    public DrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.custom_layout, parent, false);
        final DrawerViewHolder holder = new DrawerViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final DrawerViewHolder holder, final int position) {

        Demo demo = data.get(position);

        String event = demo.getEventName();

        holder.textView.setText(event);

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                row_data = position;

                notifyDataSetChanged();
            }
        });

        if (row_data == position) {
            holder.linearLayout.setBackgroundColor(Color.parseColor("#7F000000"));
        } else {

        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class DrawerViewHolder extends RecyclerView.ViewHolder {

        TextView textView;
        LinearLayout linearLayout;


        public DrawerViewHolder(View itemView) {
            super(itemView);

            textView = itemView.findViewById(R.id.textView);
            linearLayout = itemView.findViewById(R.id.linear);
        }
    }

}

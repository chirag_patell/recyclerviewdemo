package com.app.recyclerviewdemo;

import android.app.Application;
import android.content.Context;

/**
 * Created by Chirag on 30-06-2017.
 */

public class VolleyApplication extends Application {

    private static VolleyApplication sInstance;

    public static final String TAG = VolleyApplication.class
            .getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    public static VolleyApplication getInstance() {
        return sInstance;
    }

    public static Context getAppContext() {
        return sInstance.getApplicationContext();
    }



}
